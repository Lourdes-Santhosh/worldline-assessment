const express = require('express');
const fs = require('fs');
const app = express();
const port = 3000;

app.use(express.json());

let users = [];
try {
  const data = fs.readFileSync('users.json');
  users = JSON.parse(data);
} catch (err) {
  console.error("Error reading users.json:", err);
}

app.post('/register', (req, res) => {
  const { username, password } = req.body;
  users.push({ username, password });
  fs.writeFileSync('users.json', JSON.stringify(users, null, 2)); 
  res.send('Registered successfully!');
});

app.post('/login', (req, res) => {
  const { username, password } = req.body;
  const user = users.find(user => user.username === username && user.password === password);
  if (user) {
    res.send('Login successful!');
  } else {
    res.status(401).send('Invalid credentials');
  }
});

app.listen(port, () => {
  console.log(`Server running on http://localhost:${port}`);
});
