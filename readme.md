# Express Registration and Login Project

This is a simple Express.js project that provides registration and login functionality. It uses a JSON file (`users.json`) to store user data locally.

## Features

- User registration: Allows users to register by providing a username and password.
- User login: Allows registered users to login by providing their username and password.
- Data storage: User data is stored locally in a JSON file (`users.json`).

## Requirements

- Node.js
- npm (Node Package Manager)

## Installation

1. Clone this repository:

    ```bash
    git clone <repository_url>
    ```

2. Navigate into the project directory:

    ```bash
    cd express-registration-login
    ```

3. Install dependencies:

    ```bash
    npm install
    ```

## Usage

1. Start the Express server:

    ```bash
    npm start
    ```

2. To register a new user, make a POST request to `http://localhost:3000/register` with a JSON payload containing the username and password:

    ```json
    {
        "username": "your_username",
        "password": "your_password"
    }
    ```

3. To login, make a POST request to `http://localhost:3000/login` with the same JSON payload.

## Example Using curl

Register a new user:

```bash
curl -X POST -H "Content-Type: application/json" -d '{"username":"your_username", "password":"your_password"}' http://localhost:3000/register
